package com.restAPI.springIntro.controllers;

import com.restAPI.springIntro.dto.CreateStudentDto;
import com.restAPI.springIntro.dto.ResponseMessageDto;
import com.restAPI.springIntro.dto.StudentDto;
import com.restAPI.springIntro.services.StudentService;
import lombok.RequiredArgsConstructor;
import org.springframework.web.bind.annotation.*;

@RestController
@RequestMapping("/students")
@RequiredArgsConstructor
public class StudentController {

    private final StudentService studentService;

//    @PostMapping("name")
//    public ResponseMessageDto getName(@RequestHeader("Accept-Language") String language, @RequestBody StudentDto studentDto) {
//
//        return language.equalsIgnoreCase("az")
//                ? new ResponseMessageDto("Salam " + studentDto.getName())
//                : new ResponseMessageDto("Hello " + studentDto.getName());
//    }

    @GetMapping("/{id}")
    public StudentDto getStudent(@PathVariable Long id){
        return studentService.getStudent(id);
    }

    @PostMapping
    public StudentDto createStudent(@RequestBody CreateStudentDto createStudentDto){
        return studentService.create(createStudentDto);
    }

    @PostMapping("/{id}")
    public StudentDto updateStudent(@PathVariable Long id , @RequestBody CreateStudentDto createStudentDto){
        return studentService.update(id, createStudentDto);
    }

    @DeleteMapping("/{id}")
    public void deleteStudent(@PathVariable Long id ){
        studentService.deleteStudent(id);
    }
}
