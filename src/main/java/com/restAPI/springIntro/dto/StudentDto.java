package com.restAPI.springIntro.dto;


import lombok.Data;

@Data
public class StudentDto {
    private String name;
    private Long id;
    private int age;
}
