package com.restAPI.springIntro.dto;

import lombok.Data;

@Data
public class CreateStudentDto {

    private String name;
    private int age;

}
