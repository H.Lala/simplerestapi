package com.restAPI.springIntro.repository;

import com.restAPI.springIntro.entity.Student;
import org.springframework.data.jpa.repository.JpaRepository;

public interface StudentRepository extends JpaRepository<Student, Long> {
}
