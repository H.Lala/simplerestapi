package com.restAPI.springIntro.services;

import com.restAPI.springIntro.dto.CreateStudentDto;
import com.restAPI.springIntro.dto.StudentDto;
import com.restAPI.springIntro.entity.Student;
import com.restAPI.springIntro.repository.StudentRepository;
import lombok.RequiredArgsConstructor;
import org.modelmapper.ModelMapper;
import org.springframework.stereotype.Service;

@Service
@RequiredArgsConstructor
public class StudentServiceImplementation implements StudentService {

    private final StudentRepository studentRepository;
    private final ModelMapper modelMapper;

    @Override
    public StudentDto create(CreateStudentDto studentDto) {
        Student student=  studentRepository.save(modelMapper.map(studentDto,Student.class));
        return modelMapper.map(student, StudentDto.class);
    }

    @Override
    public StudentDto getStudent(Long id) {
        Student student = studentRepository.findById(id)
                .orElseThrow(() -> new RuntimeException("Student not found with this id: " + id));
        return modelMapper.map(student, StudentDto.class);

    }

    @Override
    public StudentDto update(Long id, CreateStudentDto studentDto) {
        studentRepository.findById(id).orElseThrow(()->new RuntimeException("Id not found"));
        Student student = modelMapper.map(studentDto, Student.class);
        student.setId(id);
        studentRepository.save(student);
        return modelMapper.map(student, StudentDto.class);
    }

    @Override
    public void deleteStudent(Long id) {
        studentRepository.findById(id).orElseThrow(()->new RuntimeException("Id not found"));
        studentRepository.deleteById(id);
    }
}
