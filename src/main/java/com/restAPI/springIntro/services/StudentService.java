package com.restAPI.springIntro.services;

import com.restAPI.springIntro.dto.CreateStudentDto;
import com.restAPI.springIntro.dto.StudentDto;

public interface StudentService {
    StudentDto create(CreateStudentDto studentDto);

    StudentDto getStudent(Long id);

    StudentDto update(Long id, CreateStudentDto studentDto);

    void deleteStudent(Long id);

}
